<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html> 
<html> 
<head>
	<meta charset='utf-8'>
	<title>Add/Remove Premium</title>
	<link rel='stylesheet' href='gray.css'>
	
</head>
<body>
 <section id="container">
  <section id="content">
   <section id="whitebox">

	<h1>Select users to grant premium account</h1>
	<form action="premiumForm" method="post">
		<label>Username:<input type="text" id="login" name="login"/></label><br>
		<label><input type="radio" name="permissions" value="premium" checked/>Add premium account</label><br/>
		<label><input type="radio" name="permissions" value="regular"/>Delete premium account</label><br/>
		<input type="submit" value="Change account"/>
	</form>
   
   </section>
  </section>
 </section>
</body>
</html>