package repositories;

import domain.User;

public interface UserRepository {


	User checkEmail(String email);   //sprawdza, czy nie istnieje juz konto z takim adresem email
	User checkLogin(String login);	 // oraz loginem
	void add(User user);
	//void add(UserRepository user);
	
}
