package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class loginPage_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.Vector _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("<!DOCTYPE html> \n");
      out.write("<html> \n");
      out.write("<head>\n");
      out.write("\t<meta charset='utf-8'>\n");
      out.write("\t<title>Log In</title>\n");
      out.write("\t<link rel='stylesheet' href='gray.css'>\n");
      out.write("\t\n");
      out.write("</head>\n");
      out.write("<body>\n");
      out.write(" <section id=\"container\">\n");
      out.write("  <section id=\"content\">\n");
      out.write("   <section id=\"whitebox\">\n");
      out.write("   \t<form action=\"loginPage\" method=\"post\">\n");
      out.write("     <table>\n");
      out.write("\t<tr>\n");
      out.write("\t <td><h1>Hello Anonymous!</h1></td>\n");
      out.write("\t</tr>\n");
      out.write("\t<tr><td>Please log in to continue</td></tr>\n");
      out.write("\t<tr>\n");
      out.write("\t <td>Username:</td>\n");
      out.write("\t <td><input type=\"text\" name=\"login\" id=\"login\"></td>\n");
      out.write("\t</tr>\n");
      out.write("\t<tr>\n");
      out.write("\t <td>Password:</td>\n");
      out.write("\t <td><input type=\"password\" name =\"password\" id=\"password\"></td>\n");
      out.write("\t</tr>\n");
      out.write("\t<tr>\n");
      out.write("\t <td>&nbsp;</td>\n");
      out.write("\t <td><input type=\"submit\" value=\"Log in\" ></td>\n");
      out.write("\t</tr>\n");
      out.write("\t<tr>\n");
      out.write("\t <td>Don't have an account? <a href=\"newUser.jsp\">Sign up!</a></td>\n");
      out.write("\t</tr>\n");
      out.write("     </table>\n");
      out.write("    </form>\n");
      out.write("   </section>\n");
      out.write("  </section>\n");
      out.write(" </section>\n");
      out.write("</body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
